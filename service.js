(function () {
    angular
      .module('king.services.bluetoothpermission', [])
      .run(loadFunction);
  
    loadFunction.$inject = ['configService'];
  
    function loadFunction(configService) {
      // Register upper level modules
      try {
        if (configService.services && configService.services.bluetoothpermission) {
          askpermissionFunction(configService.services.bluetoothpermission.scope);
        } else {
          throw "The service is not added to the application";
        }
      } catch (error) {
        console.error("Error", error);
      }
    }

    function askpermissionFunction(scopeData) {
        if (!cordova) return;
       
        function onError(error) {
          console.error("The following error occurred: " + error);
        }
    
        function evaluateAuthorizationStatus(status) {
            console.log(status);
            switch (status) {
                case cordova.plugins.diagnostic.bluetoothState.POWERED_ON:
                    console.log("Bluetooth on");
                    requestAuthorization();
                    break;
                case cordova.plugins.diagnostic.bluetoothState.POWERED_OFF:
                    console.log("Bluetooth off");
                    navigator.notification.confirm(
                        "This app has been denied access to your location and it really needs it function properly. Would you like to switch to the app settings page to allow access?",
                        function (i) {
                            if (i == 1) {
                                cordova.plugins.diagnostic.switchToBluetoothSettings();
                            }
                        }, "Location access denied", ['Yes', 'No']);
                    break;
                case cordova.plugins.diagnostic.bluetoothState.UNKNOWN :
                    console.log("Unknown status");
                    break;
                
                default:
                    console.log("EN DEFAULT");
                    break;
            }
        }
    
        function checkAuthorization() {
            cordova.plugins.diagnostic.getBluetoothState(evaluateAuthorizationStatus, onError);
        }
        
        checkAuthorization();
    
      }
      // --- End servicenameController content ---
    })();