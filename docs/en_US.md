# Documentation:

This plugin allows you to ask the user for the bluetooth permission.

### Configuration:

This plugin is plug and play, so no prior configuration is necessary.
